binoculars (0.0.10-1) unstable; urgency=medium

  [ Neil Williams ]
  * Update control to add Debian PaN maintainers

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 0.0.10
  * removed useless d/gbp.conf
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.
  * Bug fix: "autopkgtest tests for all supported python3 versions but
    vtk7 extensions don't exist for all", thanks to Paul Gevers
    (Closes: #1001291).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 20 Dec 2021 09:48:28 +0100

binoculars (0.0.6-1) unstable; urgency=medium

  * New upstream version 0.0.6
  * Added autopkgtests

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 17 May 2021 16:07:12 +0200

binoculars (0.0.5-1) unstable; urgency=medium

  * New upstream version 0.0.5 (Closes: #923859, #924024, #986785)
  * Bumped Standards-Version to 4.3.0 (nothing to do)
  * Bumped compat level to 12.
  * d/control
    Build-Depends:
    - Removed: python3-tables
    - Added: python3-h5py, python3-pymca5, python3-vtk7
  * Upgrade to newer source format 3.0 (quilt).
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.5.0, no changes needed.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 14 Mar 2019 10:12:40 +0100

binoculars (0.0.4-1) unstable; urgency=medium

  * Imported upstream v0.0.4.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 18 Feb 2019 15:00:45 +0100

binoculars (0.0.3-1) unstable; urgency=medium

  * Imported upstream v0.0.3.
  * d/watch: Updated to target Github release tags.
  * d/control: Homepage point to Github.com/picca/binoculars.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 07 Dec 2018 11:55:41 +0100

binoculars (0.0.2-1) unstable; urgency=medium

  * Initial release (Closes: #910077)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 25 Nov 2015 14:25:10 +0200
